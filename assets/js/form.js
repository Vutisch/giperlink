$( document ).ready(function() {
    $('#myForm label').on('change', function() {
        event.stopPropagation();
        event.preventDefault();
        var name =  $('#name').val();
        var weight = $('#weight').val();
        var phone =  $('#phone').val();
        if(reg_test('name', name) == false)
             return false;
        if(reg_test('phone', phone) == false)
            return false;
        if(reg_test('weight', weight)  == false)
            return false;
        var valueDelivery = $('input[name=deliveryradio]:checked', '#myForm').val();
        var areaDelivery = $('input[name=optradio]:checked', '#myForm').val();
        $("#hide_post").hide();
        $("#hide_couriers").hide();
        $("#hide_pickup").hide();
        switch (valueDelivery) {
            case 'post':
                $("#hide_couriers").show();
                $('input[name="optradio"]').prop('checked', false);
                document.getElementById("myspan").textContent='0';
                break;
            case 'couriers':
                $("#hide_couriers").show();
                $('input[name="optradio"]').prop('checked', false);
                document.getElementById("myspan").textContent='0';
                break;
            case 'pickup':
                $("#hide_pickup").show();
                break;
            default:
                break;
        }
    });

function reg_test(id, val) {
    var $el = $('#' + id);
    switch(id) {
        case 'name':
            var rv_name = /^[a-zA-Zа-яА-Я]+$/;

            if(val.length >= 2 && val != '' && rv_name.test(val)) {
                $el.removeClass('error');
            } else {
                $el.addClass('error');
               alert('Введите корректное имя');
                return false;
            }
            break;
        case 'phone':
            var rv_tel =  /((\+\d{1,3})\s?\(?\d{1,3}\)?\s?\d{3}\-?\d{2}\-?\d{2})/;
            if(val != '' && rv_tel.test(val)) {
                $el.removeClass('error')
            } else {
                $el.addClass('error');
                alert('Введите валидный номер   ');
                return false;
            }
            break;
        case 'weight':
            var rv_weight =  /[0-9]*[.,]?[0-9]/;
            if(val != '' && rv_weight.test(val)) {
                $el.removeClass('error')
            } else {
                $el.addClass('error');
                alert('Введите валидный вес');
                return false;
            }
            break;
    }
}
});

$(document).ready(function () {
    $('input[type=radio][name=optradio],#weight').change(function() {
        event.stopPropagation();
        $.ajax({
            type: 'POST',
            cache: false,
            url: "cost.php",
            data: {'area': $('input[name=optradio]:checked', '#myForm').val(),
                   'value': $('input[name=deliveryradio]:checked', '#myForm').val(),
                   'weight': weight = $('#weight').val()},
            success     : function(data) {
                document.getElementById("myspan").textContent=data;
            }
        }).done(function() {
            console.log('success');
        }).fail(function() {
            console.log('fail');
        });
    });
});
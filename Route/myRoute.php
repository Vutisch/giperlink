<?php

class myRoute {

    /**
     * @param $view
     * @param array $vars
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render($view, $vars= array())
    {
        require_once 'vendor/autoload.php';
        $loader = new Twig_Loader_Filesystem('/');

        $twig = new Twig_Environment($loader);

        $template = $twig->loadTemplate('Resources/'.$view.'.html.twig');

        echo $template->render(array('comments' => $vars));

    }

}



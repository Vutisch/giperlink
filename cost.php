<?php

static  $costArray = array(
    'couriers' => [
        'town'=> 5,
        'district'=> 10,
        'region'=> 20,
        'country'=>40],
    'post' =>[
        'town'=> 2,
        'district'=> 5,
        'region'=> 10,
        'country'=>20],
    'pickup' =>['pickup' => 1],
);

$delivery = $_POST['value'];
$area = $_POST['area'];
$weight = $_POST['weight'];

if($weight <=1) {
    $k=1;
} elseif ($weight <=5) {
    $k=1.2;
} elseif ($weight <=15 ) {
    $k=1.5;
} else {
    $k=2;
}

echo $k*$costArray[$delivery][$area];
